# README Boilerplate

A template of README best practices to make your README simple to understand and easy to use. 

## Table of Contents

- [إنشاء حساب جديد](#newaccount)
- [إنشاء بريد إلكتروني آمن](#usage)
- [Support](#support)
- [إلغاء مشاركة الإيميل ورقم الهاتف](#contacts-privacy)

## إنشاء حساب جديد

Download to your project directory, add `README.md`, and commit:

```sh
curl -LO http://git.io/Xy0Chg
git add README.md
git commit -m "Use README Boilerplate"
```

## Usage

Replace the contents of `README.md` with your project's:

- Name
- Description
- Installation instructions
- Usage instructions
- Support instructions
- Contributing instructions
- Licence

Feel free to remove any sections that aren't applicable to your project.

## Support

Please [open an issue](https://github.com/fraction/readme-boilerplate/issues/new) for support.

## إلغاء مشاركة الإيميل ورقم الهاتف

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/). Create a branch, add commits, and [open a pull request](https://github.com/fraction/readme-boilerplate/compare/).
